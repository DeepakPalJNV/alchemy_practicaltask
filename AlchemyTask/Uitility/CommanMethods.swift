
import UIKit
class CommanMethods: NSObject {
    static let shared : CommanMethods = CommanMethods()
    
    
    class func isValidEmail(_ Email:String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let valid = emailTest.evaluate(with:Email)
        
        return valid
    }
    class func ShowAlert(vc: UIViewController, title: String = "", message:String, cancelButton: String = "OK", completion: @escaping () -> (Void) = { }){
        let alertView = UIAlertController(title: title, message: message , preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: cancelButton, style: .default, handler: nil))
        vc.present (alertView, animated: true, completion: nil)
        
    }
    class func dateToStringConversion(date :Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = formatter.string(from: date) // string purpose I add here
        let dateToConvert = formatter.date(from: dateString)
        formatter.dateFormat = "dd-MM-yyyy"
        let myStringDate = formatter.string(from: dateToConvert!)
        return myStringDate
    }
    
    class func stringDateToTimeConversion(dateString:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        let date = dateFormatter.date(from:dateString)!

        dateFormatter.dateFormat = "dd MMM yyyy"
        let myStringTime = dateFormatter.string(from: date)
        return myStringTime
    }
    class func stringDateToDateConversion(stringDate :String) -> Date {
       let dateFormatter = DateFormatter()
       //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
       dateFormatter.dateFormat = "dd-MM-yyyy"
       let date = dateFormatter.date(from:stringDate)!
       let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second, .nanosecond], from: date)

       return calendar.date(from:components)!
   }
    class func stringDateOfDataToDateConversion(stringDate :String) -> Date {
       let dateFormatter = DateFormatter()
       //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
       dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
       let date = dateFormatter.date(from:stringDate)!
       let calendar = Calendar.current
       let components = calendar.dateComponents([.year, .month, .day], from: date)

       return calendar.date(from:components)!
   }
    class func dateToStringForPickerConversion(date :Date) -> String {
       let formatter = DateFormatter()
       formatter.dateFormat = "yyyy-MM-dd"
       let dateString = formatter.string(from: date) // string purpose I add here
       let dateToConvert = formatter.date(from: dateString)
       formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
       let myStringDate = formatter.string(from: dateToConvert!)
       return myStringDate
   }
}
