//
//  FirstViewController.swift
//  Astroveda
//
//  Created by Deepak Pal on 26/08/20.
//  Copyright © 2020 Deepak Pal. All rights reserved.
//

import UIKit
extension UIView {
    
    
   @IBInspectable var cornerRadius: Double {
        get {
          return Double(self.layer.cornerRadius)
        }set {
          self.layer.cornerRadius = CGFloat(newValue)
        }
   }
    

   @IBInspectable var borderWidth: Double {
         get {
           return Double(self.layer.borderWidth)
         }
         set {
          self.layer.borderWidth = CGFloat(newValue)
         }
   }
   @IBInspectable var borderColor: UIColor? {
        get {
           return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
           self.layer.borderColor = newValue?.cgColor
        }
   }
   @IBInspectable var shadowColor: UIColor? {
       get {
          return UIColor(cgColor: self.layer.shadowColor!)
       }
       set {
          self.layer.shadowColor = newValue?.cgColor
       }
   }
   @IBInspectable var shadowOpacity: Float {
       get {
          return self.layer.shadowOpacity
       }
       set {
          self.layer.shadowOpacity = newValue
      }
   }
    @IBInspectable var shadowRadius: Double {
        get {
            return Double(Float(self.layer.shadowRadius))
        }
        set {
           self.layer.shadowRadius = CGFloat(newValue)
       }
    }
    @IBInspectable var shadowOffset: CGSize {
      get {
        return layer.shadowOffset
      }
      set {
        layer.shadowOffset = newValue
      }
    }
    @IBInspectable var makeCircle : Bool {
        get {
            return self.makeCircle
        } set {
            if newValue {
                self.layer.cornerRadius=self.frame.height/2
            } else {
                self.layer.cornerRadius=0
            }
        }
    }
    
  
}

