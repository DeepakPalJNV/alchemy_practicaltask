
//  AppConstant.swift
//  AlchemyTask
//
//  Created by Deepak Pal on 12/04/21.
//


import UIKit

let App_Delegate       =   UIApplication.shared.delegate as! AppDelegate
let NS_Defaults        =   UserDefaults.standard
let NS_Notification    =   NotificationCenter.default
let File_Manager       =   FileManager.default
let WhiteSpaceSet      =   NSCharacterSet.whitespacesAndNewlines
let kUserInfo          =   "userinfo"
let kIsLogin           =   "IsLogin"
let ACCEPTABLE_CHARACTERS = "0123456789"

var appDelegateShared: AppDelegate {
    return UIApplication.shared.delegate as? AppDelegate ?? AppDelegate()
}

class AppConstant: NSObject {
    static let share :AppConstant = AppConstant()

    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    
    func showActivityIndicator() {
        DispatchQueue.main.async { [self] in
            container.frame = appDelegateShared.window!.frame
            container.center = appDelegateShared.window!.center
            container.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.3)
            
            loadingView.frame = CGRect(x:0, y:0, width:100, height:100)
            loadingView.center = appDelegateShared.window!.center
            loadingView.backgroundColor = UIColorFromHex(rgbValue: 0x444444, alpha: 0.7)
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            
            activityIndicator.frame = CGRect(x:0.0, y:0.0, width:40.0, height:40.0);
            activityIndicator.style = UIActivityIndicatorView.Style.large
            activityIndicator.color = UIColor.white
            activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2);
            
            loadingView.addSubview(activityIndicator)
            container.addSubview(loadingView)
            appDelegateShared.window!.addSubview(container)
            activityIndicator.startAnimating()
        }
  }
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.container.removeFromSuperview()
            appDelegateShared.window!.removeFromSuperview()
            
        }
    }
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}
