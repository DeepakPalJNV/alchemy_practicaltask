//
//  ProductCell.swift
//  AlchemyTask
//
//  Created by Deepak Pal on 12/04/21.
//

import UIKit

class ProductCell: UITableViewCell {
    
   //@IBOutlet var imgProduct: AlchemyImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblProductPrice: UILabel!
    @IBOutlet var lblProductMarchent: UILabel!
    
    @IBOutlet var lblDate: UILabel!
}
