//
//  ProductCell.swift
//  AlchemyTask
//
//  Created by Deepak Pal on 12/04/21.
//

import UIKit

class FilterCell: UITableViewCell {
    
   //@IBOutlet var imgProduct: AlchemyImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var btnCheck: UIButton!
}
