//
//  productData.swift
//  AlchemyTask
//
//  Created by Deepak Pal on 12/04/21.
//
import Foundation
struct productData: Codable {
        var id, createdAt, name: String?
        var image: String?
        var price, merchant: String?
}


class productModel: NSObject {
    
    static func productResponseData(_ response: [[String: Any]]) -> [productData]? {
        var arr: [productData]?
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
            arr = try JSONDecoder().decode([productData].self, from: jsonData)
        } catch {
            print(error.localizedDescription)
        }
        return arr
    }
    
    
}
