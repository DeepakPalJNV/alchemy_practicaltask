//
//  PasscodeVC.swift
//  AlchemyTask
//
//  Created by Deepak Pal on 12/04/21.
//

import Foundation
import UIKit

class PasscodeVC: UIViewController , UITextFieldDelegate{
    //MARK:- Constants
    
    //MARK:- IBOutlet
    @IBOutlet var txtFirst: UITextField!
    @IBOutlet var txtSecound: UITextField!
    @IBOutlet var txtThird: UITextField!
    @IBOutlet var txtFour: UITextField!
    //MARK:- Variable
    
    
    //MARK:- View Method
    override func viewDidLoad()  {
        txtFirst .becomeFirstResponder()
        txtFirst.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtSecound.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtThird.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtFour.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    //MARK:- TextField Delegate Method
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        return  (string == filtered)
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case txtFirst:
                txtSecound.becomeFirstResponder()
            case txtSecound:
                txtThird.becomeFirstResponder()
            case txtThird:
                txtFour.becomeFirstResponder()
            case txtFour:
                txtFour.resignFirstResponder()
                self.pushtoHome()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case txtFirst:
                txtFirst.becomeFirstResponder()
            case txtSecound:
                txtFirst.becomeFirstResponder()
            case txtThird:
                txtSecound.becomeFirstResponder()
            case txtFour:
                txtThird.becomeFirstResponder()
            default:
                break
            }
        }else{
            if (textField.text?.count)! > 1  {
                textField.text = ""
            }
        }
        
    }
    func pushtoHome() {
        if txtFirst.text?.count == 1 &&   txtSecound.text?.count == 1 && txtThird.text?.count == 1{
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as? HomeVC
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}

