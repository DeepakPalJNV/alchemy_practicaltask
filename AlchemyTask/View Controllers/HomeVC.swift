//
//  HomeVC.swift
//  AlchemyTask
//
//  Created by Deepak Pal on 12/04/21.
//

import Foundation
import UIKit

class HomeVC: UIViewController {
    //MARK:- Constants
    private let refreshControl = UIRefreshControl()

    //MARK:- IBOutlet
   
    @IBOutlet var popupView: UIView!
    @IBOutlet var tblView: UITableView!
    //MARK:- Variable
    var productList: [productData]?
    var productListToShare: [productData]?
    
    var arrFilteredProductList: [productData]?

    //MARK:- View Method
    override func viewDidLoad()  {
        self.getAllProductList()
        tblView.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        self.addPullToRefresh()
        arrFilteredProductList = []

    }
  
    func addPullToRefresh() {
        refreshControl.tintColor = UIColor.systemBlue
        if #available(iOS 10.0, *) {
            tblView.refreshControl = refreshControl
        } else {
            tblView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshProductData(_:)), for: .valueChanged)
    }
    
    //MARK:- Button Actions
    
    @objc private func refreshProductData(_ sender: Any) {
        // Fetch latest Data
        self.productList?.removeAll()
        self.arrFilteredProductList?.removeAll()
        self.getAllProductList()
    }
    
    @IBAction func btnFilterClicked(_ sender: Any) {
        guard productListToShare!.count >= 1 else {
            return
        }
        self.productList?.removeAll()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [self] in
            self.tblView.reloadData()
        }
        let popupVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FilterPopup") as? FilterPopup
        popupVC?.modalPresentationStyle = .popover
        popupVC?.modalTransitionStyle = .coverVertical
        popupVC?.productListForFilter = productListToShare
        popupVC?.completion = { arr in
            self.arrFilteredProductList = arr
            if arr?.count != 0  {
                self.productList?.removeAll()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { 
                self.tblView.reloadData()
            }
        }
        self.navigationController?.present(popupVC!, animated: true)
               
    }
    
    //MARK:- Product list 
    
    func getAllProductList() {
      let session = URLSession.shared
     
     AppConstant.share.showActivityIndicator()
      let url = URL(string: "https://606ab783e1c2a10017545e8e.mockapi.io/product/productlist")
      var request = URLRequest(url: url!)
      request.httpMethod = "GET"

      request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    
      let task = session.dataTask(with: request, completionHandler: {
          (responseData, response, error) in

        if let responseData = responseData {
          do {
            AppConstant.share.hideActivityIndicator()

            let responseObject = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments)
            
            self.productList =  productModel.productResponseData((responseObject) as! [[String: Any]])
            
            DispatchQueue.main.async { [self] in
                self.productList = productList!.sorted(by: {
                            (CommanMethods.stringDateOfDataToDateConversion(stringDate: $0.createdAt!)).compare((CommanMethods.stringDateOfDataToDateConversion(stringDate: $1.createdAt!))) == .orderedDescending
                        })
                self.productListToShare = self.productList
                self.tblView.reloadData()
                self.refreshControl.endRefreshing()

            }
              print("JSON: \(responseObject)")
          } catch let error {
            AppConstant.share.hideActivityIndicator()
              print("Error: \(error)")
            
          }
            AppConstant.share.hideActivityIndicator()
          }
      })

      task.resume()
    }

}
//MARK:- Tableview Delegate and DataSource 

extension HomeVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if arrFilteredProductList?.count != 0  {
                return self.arrFilteredProductList?.count ?? 0
            }else{
              return self.productList?.count ?? 0
            }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
      
           if arrFilteredProductList?.count != 0  {
            if let product = arrFilteredProductList?[indexPath.row] {
                cell.lblProductName.text = product.name
                cell.lblProductPrice.text = "₹ " + product.price!
                cell.lblProductMarchent.text = product.merchant
                cell.lblDate.text = CommanMethods.stringDateToTimeConversion(dateString: product.createdAt!)
               // cell.imgProduct.setImageWithUrl(imgUrl: product.image)
            }
            return cell
        }else{
            if productList?.count != 0 {
                if let product = productList?[indexPath.row] {
                    cell.lblProductName.text = product.name
                    cell.lblProductPrice.text = "₹ " + product.price!
                    cell.lblProductMarchent.text = product.merchant
                    cell.lblDate.text = CommanMethods.stringDateToTimeConversion(dateString: product.createdAt!)
                }
            }
            return cell
        }
    
    }
    
    
}
