//
//  ViewController.swift
//  AlchemyTask
//
//  Created by Deepak Pal on 12/04/21.
//

import UIKit

class LoginVC: UIViewController {
    //MARK:- Constants
    let width = 15   // Spark Width
    //MARK:- IBOutlet
    
    @IBOutlet var txtEmail: UITextField!
    //MARK:- Variable
    var arrSpark = [4,6,8,2,1,1,3,10,4,3]  // Update spark array

    override func viewDidLoad() {
        super.viewDidLoad()
        logicalTask()
        // Do any additional setup after loading the view.
    }
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        guard  CommanMethods.isValidEmail(txtEmail.text!.trimmingCharacters(in: WhiteSpaceSet)) else {
            CommanMethods.ShowAlert(vc: self, message: "Please enter valid Email Address")
            return
        }
   
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PasscodeVC") as? PasscodeVC
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    
    // MARK : Logical Task
    
    func logicalTask()  {
        var str = String()
        for length in arrSpark {
            if width - 1 > length {
                if str.count == length {
                    print("Sequence of string at 1", str)
                }else if (str.count + length) < width - 1 {
                    str += "X" +  String(repeating: "~", count: length)
                    print("Sequence of string at 2", str)
                }else if width < str.count  {
                    str += "X" +  String(repeating: "~", count: length)
                    print("Sequence of string at 3", str)
                }else if (str.count + length) == width - 1{
                    str += "X" +  String(repeating: "~", count: length)
                }else if (str.count + length) > width - 1{
                    let nuZero = width - str.count
                    str += String(repeating: "0", count: nuZero)
                    print("Sequence of string at 5", str)
                    str = ""
                    str = "X" +  String(repeating: "~", count: length)
                    print("Sequence of string at 6", str)
                }else{
                    str = "X" +  String(repeating: "~", count: length)
                    print("Sequence of string at 6", str)
                    }
            }else if (str.count + length < width - 1){
                let nuZero = width - str.count
                str += String(repeating: "0", count: nuZero)
                print("Sequence of string at 7", str)
                str = ""
            }else if width - 1 == length {
                if (str.count >= 1){
                    let nuZero = width - str.count
                    str += String(repeating: "0", count: nuZero)
                    print("Sequence of string at 8", str)
                    str = ""
                    str = "X" +  String(repeating: "~", count: length)
                    print("Sequence of string at 9", str)
                 }else{
                    str = "X" +  String(repeating: "~", count: length)
                    print("Sequence of string at 10", str)
                 }
                str = ""
            }else if width  == length {
                str = "X" +  String(repeating: "~", count: length)
                print("Sequence of string at 10", str)
                str = ""
            }else if width - 1 >= length{
                let nuZero = width - str.count
                str += String(repeating: "0", count: nuZero)
                print("Sequence of string at 11", str)
            }
        }
        let lastIndex = arrSpark.last { (arrSpark) -> Bool in
            let nuZero = width - str.count
            str += String(repeating: "0", count: nuZero)
            print("Sequence of string at 12", str)
            return true
        }
    }
}

