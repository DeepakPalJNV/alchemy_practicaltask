//
//  HomeVC.swift
//  AlchemyTask
//
//  Created by Deepak Pal on 12/04/21.
//

import Foundation
import IQKeyboardManagerSwift
import UIKit
class FilterPopup: UIViewController {
    typealias complectionHandler = ([productData]?)  -> Void
    
    
    //MARK:- Constants
    let txtDob_Tag = 100
    let txtTob_Tag = 101
    let reduce = 0
    //MARK:- IBOutlet
    
    @IBOutlet var txtStatrDate: UITextField?
    
    @IBOutlet var ViewGeneral: UIView!
    @IBOutlet var generalSort: UIView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var txtEndDate: UITextField?
    
    @IBOutlet var slider: RangeSeekSlider!
    @IBOutlet var lblSide: UILabel!
    @IBOutlet var SideLblCenterContraints: NSLayoutConstraint!
    
    @IBOutlet var lblSideGeneral: UILabel!
    @IBOutlet var lblSideCetegory: UILabel!
    @IBOutlet var btnSortBy: UIButton!
    @IBOutlet var lblSideMerchant: UILabel!
    @IBOutlet var btnGeneral: UIButton!
    @IBOutlet var btnCetegory: UIButton!
    @IBOutlet var btnMerchant: UIButton!
    
    @IBOutlet var viewMerchant: UIView!
    @IBOutlet var tblMerchant: UITableView!
    //MARK:- Variable
    var pickerDate = UIDatePicker()
    var pickerendDate = UIDatePicker()
    var productListForFilter: [productData]!
    var completion : complectionHandler?
    var arrAfterFilter :[productData]?
    var toolBar = UIToolbar()
    var sliderMinValue :String = ""
    var sliderMaxValue :String = ""
    var arrCategory :[String]?
    var lastselectedIndex :Int = -1
    var arrSortBy =  ["Oldest First","Value Low to High", "Value High to Low"]
    var arrfilterMerchant : [String]?
    var selectedRows = [IndexPath]()
    
    //MARK:- View Method
    override func viewDidLoad()  {
        
        //disable key board
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
        
        self.setupDatePicker()
        tblView.register(UINib(nibName: "FilterCell", bundle: nil), forCellReuseIdentifier: "FilterCell")
        self.viewMerchant.isHidden = true
        tblMerchant.register(UINib(nibName: "FilterCell", bundle: nil), forCellReuseIdentifier: "FilterCell")
        
        basicSliderSetup()
        removeDuplicateData()
        self.arrfilterMerchant = []
        
        self.generalSort.isHidden = true
        self.viewMerchant.isHidden = true
    }
    
    func removeDuplicateData(){
        let reduce = productListForFilter!.reduce (into: [String: Int] ()) {$0 [$1.merchant!] = ($0 [$1.merchant] ?? 0) + 1} .filter {$0.1>1} .keys
        arrCategory = reduce.compactMap{$0}
    }
    
    func basicSliderSetup()  {
        slider.delegate = self
        slider.minValue = 10
        slider.maxValue = 1000
        slider.selectedMinValue = 100
        slider.selectedMaxValue = 800
        slider.selectedHandleDiameterMultiplier = 1.0
        slider.lineHeight = 10.0
        
    }
    //MARK:- PickerView Setup
    
    func setupDatePicker(){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "mm:dd:yyyy"
        let date = dateFormatter.date(from: "01:01:2020")
        let components = Calendar.current.dateComponents([.year, .month, .day], from: date!)
        
        let maxDate = NSCalendar.current.date(byAdding: components as DateComponents, to: NSDate() as Date)
        //pickerDate.preferredDatePickerStyle = .compact
        pickerDate.preferredDatePickerStyle = .wheels
        pickerendDate.preferredDatePickerStyle = .wheels
        
        pickerDate.tag    = 100
        pickerendDate.tag = 200
        
        pickerDate.maximumDate = maxDate
        pickerDate.date = date!
        pickerDate.datePickerMode = .date
        txtStatrDate?.inputView = pickerDate
        createToolBar()
        
        pickerendDate.maximumDate = maxDate
        pickerendDate.date = date!
        pickerendDate.datePickerMode = .date
        
        txtEndDate?.inputView = pickerendDate
        txtEndDate?.inputView = pickerendDate
        txtStatrDate?.inputAccessoryView = toolBar
        txtEndDate?.inputAccessoryView = toolBar
        
        pickerDate.addTarget(self, action: #selector(self.datePickerValueChanged(datePicker:)), for: .valueChanged)
        pickerendDate.addTarget(self, action: #selector(self.datePickerValueChanged(datePicker:)), for: .valueChanged)
    }
    
    @objc func datePickerValueChanged(datePicker: UIDatePicker) {
        let date = CommanMethods.stringDateToDateConversion(stringDate: "31-12-2019")
        if datePicker.tag == 100 {
            if datePicker.date > date  {
                txtStatrDate?.text = CommanMethods.dateToStringConversion(date: datePicker.date)
            }else{
                CommanMethods.ShowAlert(vc: self, message: "Please select date greater than Jan 2020")
                txtStatrDate?.text = ""
            }
        }else{
            txtEndDate?.text = CommanMethods.dateToStringConversion(date: datePicker.date)
        }
    }
    
    
    func createToolBar() {
        toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
        
        let todayButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelButtonPressed(sender:)))
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed(sender:)))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width/3, height: 40))
        label.text = "Choose your Date"
        let labelButton = UIBarButtonItem(customView:label)
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        
        toolBar.setItems([todayButton,flexibleSpace,labelButton,flexibleSpace,doneButton], animated: true)
    }
    
    @objc func doneButtonPressed(sender: UIBarButtonItem) {
        txtStatrDate?.resignFirstResponder()
        txtEndDate?.resignFirstResponder()
    }
    
    @objc func cancelButtonPressed(sender: UIBarButtonItem) {
        txtStatrDate?.resignFirstResponder()
        txtEndDate?.resignFirstResponder()
    }
    
    
    //MARK:- Button Action
    
    @IBAction func btnSortByClicked(_ sender: Any) {
        txtStatrDate?.resignFirstResponder()
        txtEndDate?.resignFirstResponder()
        UIView.animate(withDuration: 0.3) {
            self.lblSide.isHidden = false
            self.lblSideGeneral.isHidden = true
            self.lblSideCetegory.isHidden = true
            self.lblSideMerchant.isHidden = true
            self.ViewGeneral.isHidden = false
            self.viewMerchant.isHidden = true
            self.generalSort.isHidden = true
        }
    }
    
    @IBAction func btnGeneralClicked(_ sender: Any) {
        txtStatrDate?.resignFirstResponder()
        txtEndDate?.resignFirstResponder()
        UIView.animate(withDuration: 0.3) {
            self.lblSide.isHidden = true
            self.lblSideGeneral.isHidden = false
            self.lblSideCetegory.isHidden = true
            self.lblSideMerchant.isHidden = true
            self.viewMerchant.isHidden = true
            self.ViewGeneral.isHidden = true
            self.generalSort.isHidden = false
            
            
        }
    }
    
    @IBAction func btnCetegoryClicked(_ sender: Any) {
        
    }
    
    @IBAction func btnMerchantClicked(_ sender: Any) {
        txtStatrDate?.resignFirstResponder()
        txtEndDate?.resignFirstResponder()
        UIView.animate(withDuration: 0.3) {
            self.lblSide.isHidden = true
            self.lblSideGeneral.isHidden = true
            self.lblSideCetegory.isHidden = true
            self.lblSideMerchant.isHidden = false
            self.viewMerchant.isHidden = false
            self.ViewGeneral.isHidden = true
            self.generalSort.isHidden = true
            
        }
    }
    
    @IBAction func btnResetClicked(_ sender: Any) {
        
        arrAfterFilter?.removeAll()
        self.dismiss(animated: true, completion: nil)
        guard let completionBlock = completion else {
            return
        }
        completionBlock(productListForFilter)
    }
    
    @IBAction func btnApplyClicked(_ sender: Any) {
        
        if generalSort.isHidden == false {
            guard let completionBlock = completion else {
                return
            }
            completionBlock(arrAfterFilter!)
            
        }else if ViewGeneral.isHidden == false{
            guard txtStatrDate?.text != "" else {
                CommanMethods.ShowAlert(vc: self, message: "Please select Start date")
                return
            }
            guard txtEndDate?.text != "" else {
                CommanMethods.ShowAlert(vc: self, message: "Please select End date")
                return
            }
            guard sliderMaxValue != "" else {
                CommanMethods.ShowAlert(vc: self, message: "Please select price range")
                return
            }
            guard sliderMinValue != "" else {
                CommanMethods.ShowAlert(vc: self, message: "Please select price range")
                return
            }
            generalFilterApply()
        }else if viewMerchant.isHidden == false{
            arrayfilterByMerchant()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func arrayfilterByMerchant()  {
        arrAfterFilter?.removeAll()
        arrAfterFilter = productListForFilter!.filter({ (arrfilterMerchant?.contains(($0.merchant)!) ?? true) })
        let filteredBooks = productListForFilter!.filter( {arrfilterMerchant!.contains($0.merchant!) })
        guard let completionBlock = completion else {
            return
        }
        completionBlock(arrAfterFilter!)
    }
    
    func generalFilterApply()  {
        arrAfterFilter?.removeAll()
        let startDate = CommanMethods.stringDateToDateConversion(stringDate: (txtStatrDate?.text)!)
        let endDate = CommanMethods.stringDateToDateConversion(stringDate: (txtEndDate?.text)!)
        
        
        let contentBlocks = productListForFilter!.filter{(CommanMethods.stringDateOfDataToDateConversion(stringDate: $0.createdAt!)) >= startDate && (CommanMethods.stringDateOfDataToDateConversion(stringDate: $0.createdAt!)) < endDate }
        
        
        self.arrAfterFilter = contentBlocks.filter{$0.price! >= sliderMinValue && $0.price! < sliderMaxValue}
        
        guard let completionBlock = completion else {
            return
        }
        completionBlock(arrAfterFilter!)
        
    }
}

//MARK:- TextViewDelegate Delegate and DataSource


extension FilterPopup : UITextViewDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text != nil) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = UIColor.lightGray
            textView.text = ""
        }
    }
}

//MARK:- Tableview Delegate and DataSource

extension FilterPopup : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 10 {
            return arrSortBy.count
        }else{
            return arrCategory?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 10 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell") as! FilterCell
            cell.lblProductName.text = arrSortBy[indexPath.row]
            if cell.btnCheck.isSelected == true {
                if self.lastselectedIndex == indexPath.row {
                    cell.btnCheck.setImage(#imageLiteral(resourceName: "radio"), for: .selected)
                }else{
                    cell.btnCheck.isSelected = false
                    cell.btnCheck.setImage(#imageLiteral(resourceName: "unCheckRadio"), for: .normal)
                }
            }else{
                cell.btnCheck.setImage(#imageLiteral(resourceName: "unCheckRadio"), for: .normal)
            }
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(btnCheckClicked), for: .touchUpInside)
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell") as! FilterCell
            cell.lblProductName.text = arrCategory?[indexPath.row]
            cell.btnCheck.tag = indexPath.row
            
            cell.btnCheck.addTarget(self, action: #selector(btnMerchantCheckClicked), for: .touchUpInside)
            cell.btnCheck.isSelected = false
            if selectedRows.contains(indexPath) {
                cell.btnCheck.isSelected = true
            }
            if cell.btnCheck.isSelected == true {
                cell.btnCheck.setImage(#imageLiteral(resourceName: "check"), for: .selected)
            }else{
                cell.btnCheck.setImage(#imageLiteral(resourceName: "unCheck"), for: .normal)
            }
            return cell
        }
        
    }
    
    @objc func btnMerchantCheckClicked(sender: UIButton) {
        arrAfterFilter?.removeAll()
        sender.isSelected = !sender.isSelected
        let value =  arrCategory![sender.tag]
        
        
        
        let point = sender.convert(CGPoint.zero, to: tblMerchant)
        let indxPath = tblMerchant.indexPathForRow(at: point)
        if selectedRows.contains(indxPath!) {
            selectedRows.remove(at: selectedRows.firstIndex(of: indxPath!)!)
            if arrfilterMerchant!.contains(value) {
                for i in (0..<arrfilterMerchant!.count){
                    if arrfilterMerchant![i] == value {
                        arrfilterMerchant!.remove(at:i)
                        return
                    }
                }
            }
        } else {
            self.arrfilterMerchant?.append(value)
            selectedRows.append(indxPath!)
        }
        tblMerchant.reloadData()
        
        arrayfilterByMerchant()
    }
    
    @objc func btnCheckClicked(sender: UIButton) {
        arrAfterFilter?.removeAll()
        
        self.lastselectedIndex = sender.tag
        if sender.tag == 0  {
            sender.isSelected = !sender.isSelected
            arrAfterFilter = productListForFilter!.sorted { $0.createdAt! < $1.createdAt! }
        }else if sender.tag  == 1 {
            sender.isSelected = !sender.isSelected
            guard productListForFilter.count != 0 else {
                return
            }
            arrAfterFilter = productListForFilter.sorted { $0.price!  < $1.price!}
        }else if sender.tag  == 2{
            sender.isSelected = !sender.isSelected
            arrAfterFilter = productListForFilter!.sorted { $0.price! > $1.price! }
        }
        tblView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}

//MARK:- RangeSeekSlider Delegate

extension FilterPopup: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === self.slider {
            
            sliderMinValue = String(describing: minValue)
            sliderMaxValue = String(describing: maxValue)
        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}

