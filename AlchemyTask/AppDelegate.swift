//
//  AppDelegate.swift
//  AlchemyTask
//
//  Created by Deepak Pal on 10/04/21.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if #available(iOS 13, *) {
            // do only pure app launch stuff, not interface stuff
        } else {
            self.window = UIWindow()
            self.window!.makeKeyAndVisible()
            self.window!.backgroundColor = .red
        }
        IQKeyboardManager.shared.enable = true

        return true
    }

   

}

